# TP2 

[dam@web ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:3a:60:c5 brd ff:ff:ff:ff:ff:ff
    inet 192.168.108.140/24 brd 192.168.108.255 scope global noprefixroute dynamic ens33
       valid_lft 1335sec preferred_lft 1335sec
    inet6 fe80::9016:d52d:c4b7:fa86/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: ens37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:3a:60:cf brd ff:ff:ff:ff:ff:ff
    inet 192.168.139.131/24 brd 192.168.139.255 scope global noprefixroute dynamic ens37
       valid_lft 1335sec preferred_lft 1335sec
    inet6 fe80::20c:29ff:fe3a:60cf/64 scope link
       valid_lft forever preferred_lft forever
[dam@web ~]$
:rocket: 

### DB
Installed 
  mariadb-server.x86_64 1:5.5.68-1.el7

Dependency Installed:
  mariadb.x86_64 1:5.5.68-1.el7       perl-Compress-Raw-Bzip2.x86_64 0:2.061-3.el7 perl-Compress-Raw-Zlib.x86_64 1:2.061-4.el7 perl-DBD-MySQL.x86_64 0:4.023-6.el7 perl-DBI.x86_64 0:1.627-4.el7 perl-Data-Dumper.x86_64 0:2.145-3.el7 perl-IO-Compress.noarch 0:2.061-2.el7
  perl-Net-Daemon.noarch 0:0.48-5.el7 perl-PlRPC.noarch 0:0.2020-14.el7

 mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-12-16 11:52:09 CET; 5min ago
  Process: 1562 ExecStartPost=/usr/libexec/mariadb-wait-ready $MAINPID (code=exited, status=0/SUCCESS)
  Process: 1479 ExecStartPre=/usr/libexec/mariadb-prepare-db-dir %n (code=exited, status=0/SUCCESS)
 Main PID: 1561 (mysqld_safe)
   CGroup: /system.slice/mariadb.service
           ├─1561 /bin/sh /usr/bin/mysqld_safe --basedir=/usr
           └─1726 /usr/libexec/mysqld --basedir=/usr --datadir=/var/lib/mysql --plugin-dir=/usr/lib64/mysql/plugin --log-error=/var/log/mariadb/mariadb.log --pid-file=/var/run/mariadb/mariadb.pid --socket=/var/lib/mysql/mysql.sock
           
 MariaDB [(none)]> select user, host from mysql.user;
+---------+-----------------+
| user    | host            |
+---------+-----------------+
| root    | 127.0.0.1       |
| dam_bdd | 192.168.139.131 |
| root    | ::1             |
|         | db.tp2.cesi     |
| root    | db.tp2.cesi     |
|         | localhost       |
| root    | localhost       |
+---------+-----------------+          

MariaDB [(none)]> show grants for "dam_bdd"@"192.168.139.131";
+----------------------------------------------------------------------------------------------------------------------+
| Grants for dam_bdd@192.168.139.131                                                                                   |
+----------------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'dam_bdd'@'192.168.139.131' IDENTIFIED BY PASSWORD '*A063B5CF3CAB6F1D521EFDCAFE8D1F060DB636B7' |
| GRANT ALL PRIVILEGES ON `cesi`.* TO 'dam_bdd'@'192.168.139.131'                                                      |
+-----------------------------------------------------------------------------
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens37
  sources:
  services: dhcpv6-client ssh
  ports: 8888/tcp 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
  
[dam@web test]$ ls
index.php    readme.html      wp-admin            wp-comments-post.php  wp-content   wp-includes        wp-load.php   wp-mail.php      wp-signup.php     xmlrpc.php
license.txt  wp-activate.php  wp-blog-header.php  wp-config-sample.php  wp-cron.php  wp-links-opml.php  wp-login.php  wp-settings.php  wp-trackback.php
[dam@web test]$ sudo chown -R apache: /var/www/html/test


MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| cesi               |
| mysql              |
| performance_schema |
| test               |
+--------------------+
5 rows in set (0.36 sec)
![](https://i.imgur.com/bYyamce.png)

[dam@rp nginx]$ systemctl status nginx.service
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 14:12:15 CET; 3s ago
  Process: 1878 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 1876 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 1875 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 1880 (nginx)
   CGroup: /system.slice/nginx.service
           ├─1880 nginx: master process /usr/sbin/nginx
           └─1881 nginx: worker process
[dam@rp nginx]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens37
  sources:
  services: dhcpv6-client ssh
  ports: 8888/tcp 80/tcp 443/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:

#### Bonus
  
  [dam@rp fail2ban]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 15:28:18 CET; 2s ago
     Docs: man:fail2ban(1)
  Process: 2222 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 2223 (f2b/server)
   CGroup: /system.slice/fail2ban.service
           └─2223 /usr/bin/python2 -s /usr/bin/fail2ban-server -xf start
  
  Status
|- Number of jail:      1
`- Jail list:   sshd
```

  dam@192.168.139.133's password:
Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[dam@web conf.d]$ ssh dam@192.168.139.133
ssh: connect to host 192.168.139.133 port 22: Connection refused
[dam@web conf.d]$ ssh dam@192.168.139.133
ssh: connect to host 192.168.139.133 port 22: Connection refused
[dam@web conf.d]$ ssh dam@192.168.139.133
dam@192.168.139.133's password:

```
  
```
[dam@rp fail2ban]$ ssh dam@192.168.139.131
dam@192.168.139.131's password:
Permission denied, please try again.
dam@192.168.139.131's password:
Permission denied, please try again.
dam@192.168.139.131's password:
Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[dam@rp fail2ban]$ ssh dam@192.168.139.131
ssh: connect to host 192.168.139.131 port 22: Connection refused
[dam@rp fail2ban]$ ssh dam@192.168.139.131
ssh: connect to host 192.168.139.131 port 22: Connection refused
[dam@rp fail2ban]$
```

```
dam@192.168.139.132's password:
Permission denied, please try again.
dam@192.168.139.132's password:
Permission denied, please try again.
dam@192.168.139.132's password:
Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[dam@web fail2ban]$ ssh dam@192.168.139.132
ssh: connect to host 192.168.139.132 port 22: Connection refused
[dam@web fail2ban]$ ssh dam@192.168.139.132
ssh: connect to host 192.168.139.132 port 22: Connection refused
[dam@web fail2ban]$
```


